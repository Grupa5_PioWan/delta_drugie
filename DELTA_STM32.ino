#include <math.h>
#include <stdlib.h>
#include <Servo.h>
#include <LiquidCrystal.h>


Servo s1;
Servo s2;
Servo s3;

LiquidCrystal lcd(7, 6, 5, 4, 3, 2);

const int kdol =  PC6;
const int kgora =  PC8;
const int kwstecz =  PC5;
const int kok =  PC10;

float bok_trojkata=30;
float a_prostokata=30;
float b_prostokata=30;
float r_kola=30;
float dl_lini=30;
float tymczasowy_wymiar=0;

int pdol;
int pgora ;
int pwstecz;
int pok;

int dol=0;
int gora=0;
int wstecz=0;
int ok=0;

int stanLCD=1;
int stanUstawienia=0;
int stanWymiary=0;


float t1 (float Px,float Py,float Pz){
        // deklaracja stałych
  lcd.setCursor(0, 1);
  lcd.print(Px);
  lcd.setCursor(6, 1);
  lcd.print(Py);
  lcd.setCursor(12, 1);
  lcd.print((int)Pz);
 float   e = 35.0;
 float   f = 65;
 float   re = 140.0;
 float   rf = 110.0;
 float   btf = 400.0;

 float   sqrt3 = sqrt(3.0);
 float   pi = 3.141592653;
 float   sin120 = sqrt(3)/2.0;
 float   cos120 = -0.5;



 //delklaracja zmiennych funkcyjnych

 //float Px, Py, Pz;
 float x0,  z0;
 float y1, y0, a,  b,  d,  yj, zj;
 float zke,    t1, t2, t3;


 // kinematyka
 x0=Px;
 y0=Py;
 z0=Pz;



 y1 = -0.5 * 0.57735 * f;
 y0 =y0 - 0.5 * 0.57735 * e;


 a = (x0*x0 + y0*y0 + z0*z0 +rf*rf - re*re - y1*y1)/(2.0*z0);
 b = (y1-y0)/z0;
 d = -(a+b*y1)*(a+b*y1)+rf*(b*b*rf+rf);
 // THETA 1
 if (d > 0) {
   yj = (y1 - a*b - sqrt(d))/(b*b + 1);
   zj = a + b*yj;
   if(yj>y1)
       zke=180;
   else
       zke=0;

 t1 = atan(-zj/(y1 - yj)) * 180.0/pi + zke;
 }else{
       // Serial.println(" \n zle wyniki");
       }


return t1;
}

 float t2(float Px,float Py,float Pz){
        // deklaracja stałych
 float   e = 35.0;
 float   f = 65;
 float   re = 140.0;
 float   rf = 110.0;
 float   btf = 400.0;

 float   sqrt3 = sqrt(3.0);
 float   pi = 3.141592653;
 float   sin120 = sqrt(3)/2.0;
 float   cos120 = -0.5;




 //delklaracja zmiennych funkcyjnych

 //float Px, Py, Pz;
 float x0,  z0;
 float y1, y0, a,  b,  d,  yj, zj;
 float zke,    t1, t2, t3;

 // THETA 2
 x0=Px*cos120 + Py*sin120;
 y0=Py*cos120 - Px*sin120;
 z0=Pz;


  y1 = -0.5 * 0.57735 * f;
  y0 =y0 - 0.5 * 0.57735 * e;


  a = (x0*x0 + y0*y0 + z0*z0 +rf*rf - re*re - y1*y1)/(2.0*z0);
  b = (y1-y0)/z0;
  d = -(a+b*y1)*(a+b*y1)+rf*(b*b*rf+rf);

  if (d > 0) {
    yj = (y1 - a*b - sqrt(d))/(b*b + 1);
    zj = a + b*yj;
    if(yj>y1)
        zke=180;
    else
        zke=0;

    t2 = atan(-zj/(y1 - yj)) * 180.0/pi + zke;
    }else{
       // Serial.println(" \n zle wyniki \n");
       }

  return t2;
 }

 
 float t3(float Px,float Py,float Pz){
        // deklaracja stałych
 float   e = 35.0;
 float   f = 65;
 float   re = 140.0;
 float   rf = 110.0;
 float   btf = 400.0;

 float   sqrt3 = sqrt(3.0);
 float   pi = 3.141592653;
 float   sin120 = sqrt3/2.0;
 float   cos120 = -0.5;




 //delklaracja zmiennych funkcyjnych

 //float Px, Py, Pz;
 float x0,  z0;
 float y1, y0, a,  b,  d,  yj, zj;
 float zke,    t1, t2, t3;



 
 
 
// THETA 3
   x0=Px*cos120 - Py*sin120;
   y0=Py*cos120 + Px*sin120;
   z0=Pz;


   y1 = -0.5 * 0.57735 * f;
   y0 =y0 - 0.5 * 0.57735 * e;

   a = (x0*x0 + y0*y0 + z0*z0 +rf*rf - re*re - y1*y1)/(2.0*z0);
   b = (y1-y0)/z0;
   d = -(a+b*y1)*(a+b*y1)+rf*(b*b*rf+rf);

    if (d > 0) {
    yj = (y1 - a*b - sqrt(d))/(b*b + 1);
    zj = a + b*yj;
    if(yj>y1)
        zke=180;
    else
        zke=0;

    t3 = atan(-zj/(y1 - yj)) * 180.0/pi + zke;
    }else{
        //Serial.println(" \n zle wyniki");
        }


    return t3;
}


float trojkat(float dlugosc){


      float j,r=0.2,wysokosc = -185, Px, Py , Pz=-200 , i=0;
      float H=(dlugosc*sqrt(3))/2;
      float angle1,angle2,angle3;


delay(2000);
for (j=0;j<=dlugosc;j=j+r){
    Px=j;
    Py=0;
    

       //Polozenie(i,3)=Pz;
   angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
 
       //delay(10);
       //Serial.println("twoja mama");
}
delay(2000);
for (j=0;j<=H;j=j+r){
    Px=(dlugosc)+(j*-((sqrt(3)/3)));
    Py=j;

  
   angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
        //delay(10);
        //Serial.println("twoja mama2");
}
delay(2000);
for (j=H;j>=0;j=j-r){
    Px=(j*((sqrt(3))/3));
    Py=j;

       //Polozenie(i,3)=Pz;
  
   

   angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
    //delay(10);
    //Serial.println("twoja mama3");
}
}


  void prostokat (float a,float b,float wysokosc)
{
 
  float Px;
  float Py;
  float Pz=-150;
  float i=0;
  float angle1,angle2,angle3;

  for (i=Pz;Pz>=wysokosc;i=i-1)
  {
    Pz=i;
   
    delay(10);
  }
  delay(1000);
  //Pierwszy bok
  for(i=0;i<=a;i=i+1)
  {
    Px=i;
  angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
       delay(10);
  }
  delay(1000);
  //Drugi bok
  for(i=0;i<=b;i=i+1)
  {
    Py=i;
   angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
    delay(10);
  }
  delay(1000);
  //Trzeci bok
  for(i=a;i>=0;i=i-1)
  {
    Px=i;
   angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
    delay(10);
  }
  delay(1000);
 //Czwarty bok
 for(i=b;i>=0;i=i-1)
 {
  Py=i;
  angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
  delay(10);
 }
 delay(100);
 
}


void kolo(float r,float wysokosc)
{
  
  float Px;
  float Py;
  float Pz=-155;
  float i=0;
  float pi=3.14159265359;
   float angle1,angle2,angle3;

   for (i=Pz;Pz>=wysokosc;i=i-1)
  {
    Px=r;
    Pz=i;
     angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
    
    delay(10);
  }

  for (i=0;i<=(2.2*pi);i=i+0.8)
  {
    Px=r*cos(i);
    Py=r*sin(i);
     angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
    
    delay(70);
  }

  
}

void linia(float dlugosc,float wysokosc)
{
  
  
  float Px;
  float Py;
  float Pz=-150;
  float i=0;
  float angle1,angle2,angle3;
for (i=Pz;Pz>=wysokosc;i=i-1)
  {
   Py=0;
    Pz=i;
     angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
    
    delay(10);
  }

  for(i=0;i<=dlugosc;i=i+1)
  {
    Px=i;
    Py=0;
   angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
    delay(10);
  
  
  }
  delay(1000);
  Pz=-150;
   angle1=t1(Px,Py,Pz);
   angle2=t2(Px,Py,Pz);
   angle3=t3(Px,Py,Pz);

    s1.write(angle1+80);
    s2.write(angle2+80);
    s3.write(angle3+80);
}

void setup() {
pinMode(kdol, INPUT);
pinMode(kgora, INPUT);
pinMode(kwstecz, INPUT);
pinMode(kok, INPUT);
 s3.attach(11);
 s2.attach(10);
 s1.attach(9);
 lcd.begin(16, 2);
  //Serial.begin(115200);
  //lcd.setCursor(0, 0);
  //lcd.print("Px    Py    Pz ");
}


void loop() {
   ok=0;
   
   dol = digitalRead(kdol);
   gora = digitalRead(kgora);
   wstecz = digitalRead(kwstecz);
   ok = digitalRead(kok);
   
    float Px,Py,Pz=-150;
    float angle1, angle2, angle3;
    float l = 10;
    float i;
 
  // trojkat(30);
  //kolo(30,-200);
  //prostokat (40,40,-205);
  //linia(50,-195);
switch(stanLCD)
{
  case 1: 
  lcd.setCursor(0, 0);
  lcd.print("   Robot DELTA  ");
  delay(2000);
  stanLCD=2;
  break;

  case 2:
  lcd.setCursor(0, 0);
  lcd.print("->KOLO          ");
  lcd.setCursor(0, 1);
  lcd.print("TROJKAT         ");
  if(pdol==0 && dol==1)stanLCD=3;
  
  
 break;
 
  case 3:
  lcd.setCursor(0, 0);
  lcd.print("->TROJKAT       ");
  lcd.setCursor(0, 1);
  lcd.print("PROSTOKAT       ");
  if(pdol==0 && dol==1)stanLCD=4;
  if(pgora==0 && gora==1)stanLCD=2;
 break;

 case 4:
  lcd.setCursor(0, 0);
  lcd.print("->PROSTOKAT      ");
  lcd.setCursor(0, 1);
  lcd.print("LINIA            ");
  if(pdol==0 && dol==1)stanLCD=5;
  if(pgora==0 && gora==1)stanLCD=3;
 break;

case 5:
  lcd.setCursor(0, 0);
  lcd.print("->LINIA          ");
  lcd.setCursor(0, 1);
  lcd.print("USTAWIENIA       ");
  if(pdol==0 && dol==1)stanLCD=6;
  if(pgora==0 && gora==1)stanLCD=4;
 break;

 case 6:
  lcd.setCursor(0, 0);
  lcd.print("LINIA            ");
  lcd.setCursor(0, 1);
  lcd.print("->USTAWIENIA     ");
  if(pdol==0 && dol==1)stanLCD=2;
  if(pgora==0 && gora==1)stanLCD=5;
  if(pok==0 && ok==1){stanLCD=0;stanUstawienia =1;}
 break;
 }

 switch(stanUstawienia)
 {
  case 1:
  lcd.setCursor(0, 0);
  lcd.print("Wym. Kola        ");
  lcd.setCursor(0, 1);
  lcd.print("Wym. Trokota     ");
  if(pdol==0 && dol==1)stanUstawienia=2;
  if(pwstecz==0 && wstecz==1){stanUstawienia=0;stanLCD=6;}
  
 break;
  
  case 2:
  lcd.setCursor(0, 0);
  lcd.print("->Wym. Kola      ");
  lcd.setCursor(0, 1);
  lcd.print("Wym. Trokota     ");
  if(pdol==0 && dol==1)stanUstawienia=3;
  if(pwstecz==0 && wstecz==1){stanUstawienia=0;stanLCD=6;}
  if(pok==0 && ok==1){stanUstawienia=0;stanWymiary=1;tymczasowy_wymiar=r_kola;ok=0;}
 break;

 case 3:

  lcd.setCursor(0, 0);
  lcd.print("->Wym. Trokota   ");
  lcd.setCursor(0, 1);
  lcd.print("Wym. Prostokota  ");
  if(pgora==0 && gora==1)stanUstawienia=2;
  if(pdol==0 && dol==1)stanUstawienia=4;
  if(pwstecz==0 && wstecz==1){stanUstawienia=0;stanLCD=6;}
 break;

  case 4:
  lcd.setCursor(0, 0);
  lcd.print("->Wym. Prostokota");
  lcd.setCursor(0, 1);
  lcd.print("Dl. Lini         ");
  if(pgora==0 && gora==1)stanUstawienia=3;
  if(pdol==0 && dol==1)stanUstawienia=5;
  if(pwstecz==0 && wstecz==1){stanUstawienia=0;stanLCD=6;}
 break;

 case 5:
 lcd.setCursor(0, 0);
  lcd.print("Wym. Prostokota  ");
  lcd.setCursor(0, 1);
  lcd.print("->Dl. Lini       ");
  if(pgora==0 && gora==1)stanUstawienia=4;
  if(pwstecz==0 && wstecz==1){stanUstawienia=0;stanLCD=6;}
 break;
 }
switch(stanWymiary)
{
  //Ustawianie promienia koła
  case 1:
  ok=0;
  lcd.setCursor(0, 0);
  lcd.print("Promien kola    ");
  lcd.setCursor(0, 1);
  lcd.print(tymczasowy_wymiar);
  lcd.setCursor(5, 1);
  lcd.print("mm        ");
  if(pdol==0 && dol==1){tymczasowy_wymiar--;if(tymczasowy_wymiar<=0)tymczasowy_wymiar=0;}
  if(pgora==0 && gora==1){tymczasowy_wymiar++;if(tymczasowy_wymiar>=50)tymczasowy_wymiar=50;}
  if(pwstecz==0 && wstecz==1){stanUstawienia=1;stanWymiary=0;}
  if(pok==0 && ok==1){r_kola=tymczasowy_wymiar;stanUstawienia=1;stanWymiary=0;}
  break;
  //Ustawianie boku a_prostokata
  case 2:
  lcd.setCursor(0, 0);
  lcd.print("Dlugosc a      ");
  lcd.setCursor(0, 1);
  lcd.print(tymczasowy_wymiar);
  lcd.setCursor(5, 1);
  lcd.print("mm        ");
  if(pdol==0 && dol==1){tymczasowy_wymiar--;if(tymczasowy_wymiar<=0)tymczasowy_wymiar=0;}
  if(pgora==0 && gora==1){tymczasowy_wymiar++;if(tymczasowy_wymiar>=50)tymczasowy_wymiar=50;}
  if(pwstecz==0 && wstecz==1){stanUstawienia=1;stanWymiary=0;}
  if(pok==0 && ok==1){a_prostokata=tymczasowy_wymiar;stanWymiary=3;tymczasowy_wymiar=b_prostokata;}
  break;
//Ustawianie boku b_prostokata
  case 3:
  lcd.setCursor(0, 0);
  lcd.print("Dlugosc b      ");
  lcd.setCursor(0, 1);
  lcd.print(tymczasowy_wymiar);
  lcd.setCursor(5, 1);
  lcd.print("mm        ");
  if(pdol==0 && dol==1){tymczasowy_wymiar--;if(tymczasowy_wymiar<=0)tymczasowy_wymiar=0;}
  if(pgora==0 && gora==1){tymczasowy_wymiar++;if(tymczasowy_wymiar>=50)tymczasowy_wymiar=50;}
  if(pwstecz==0 && wstecz==1){stanUstawienia=1;stanWymiary=0;}
  if(pok==0 && ok==1){b_prostokata=tymczasowy_wymiar;stanUstawienia=1;stanWymiary=0;}
  break;
//Ustawieanie wymiaru trójkąta
  case 4:
  lcd.setCursor(0, 0);
  lcd.print("Bok trokata    ");
  lcd.setCursor(0, 1);
  lcd.print(tymczasowy_wymiar);
  lcd.setCursor(5, 1);
  lcd.print("mm        ");
  if(pdol==0 && dol==1){tymczasowy_wymiar--;if(tymczasowy_wymiar<=0)tymczasowy_wymiar=0;}
  if(pgora==0 && gora==1){tymczasowy_wymiar++;if(tymczasowy_wymiar>=50)tymczasowy_wymiar=50;}
  if(pwstecz==0 && wstecz==1){stanUstawienia=1;stanWymiary=0;}
  if(pok==0 && ok==1){bok_trojkata=tymczasowy_wymiar;stanUstawienia=1;stanWymiary=0;}
  break;
}
 
pdol=dol;
 pgora=gora;
 pok=ok;
 pwstecz=wstecz;
    }
    
